﻿using UnityEngine;
using UnityEngine.Video;

public class editorVideoPlayerRef : MonoBehaviour {

	public VideoPlayer player;

	void Awake() {
		#if !UNITY_5_2
		GetComponentInChildren<editorVideoControlsManager>(true).Player = player;
		#else
		GetComponentInChildren<editorVideoControlsManager>().Player = player;
		#endif
	}
}
