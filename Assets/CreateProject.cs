﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CreateProject : MonoBehaviour {

	public InputField projectName;
	public InputField videoURL;
	public Toggle checkbox;

	public void createNew() {
		Constants.videoURL = videoURL.text;
		Constants.projectName = projectName.text;
		Constants.downloadVideo = checkbox.isOn;
		SceneManager.LoadScene ("Scene", LoadSceneMode.Single);
	}
}
