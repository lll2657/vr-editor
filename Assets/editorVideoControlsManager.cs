﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine.Networking;

public class editorVideoControlsManager : MonoBehaviour {

	public GameObject videoControls;

	private GameObject pauseSprite;
	private GameObject playSprite;

	public GameObject playBtn;
	public GameObject loading;
	public GameObject menu;
	public Camera m_camera;

	private Slider videoScrubber;
	private Vector3 basePosition;
	private Text videoPosition;
	private Text videoDuration;
	private int index;

	public GameObject canvas;
	public GameObject inputField;
	public InputField inputFieldObj;
	public GameObject textObj;

	public GameObject imgObj;

	public GameObject questionEditor;
	public InputField questionInput;
	public InputField answer1Input;
	public InputField answer2Input;
	public InputField answer3Input;
	public GameObject questionObj;
	public Text question;
	public Button answer1;
	public Button answer2;
	public Button answer3;

	public Serialization.EntireInputJSON gameData;
	public List<Serialization.InputJSON> gameDataList;

	public GUISkin skin;
	public Texture2D file,folder,back,drive;

	private List<byte[]> images;
	private List<string> names;

	FileBrowser fb = new FileBrowser();

	private string output;

	private bool prepared;
	private bool editing;
	private bool chooseImg;

	public VideoPlayer Player
	{
		set;
		get;
	}

	void Awake() {
		foreach (Text t in GetComponentsInChildren<Text>()) {
			if (t.gameObject.name == "curpos_text") {
				videoPosition = t;
			} else if (t.gameObject.name == "duration_text") {
				videoDuration = t;
			}
		}

		foreach (RawImage raw in GetComponentsInChildren<RawImage>(true)) {
			if (raw.gameObject.name == "playImage") {
				playSprite = raw.gameObject;
			} else if (raw.gameObject.name == "pauseImage") {
				pauseSprite = raw.gameObject;
			}
		}

		foreach (Slider s in GetComponentsInChildren<Slider>(true)) {
			if (s.gameObject.name == "video_slider") {
				videoScrubber = s;
				videoScrubber.maxValue = 100;
				videoScrubber.minValue = 0;
			}
		}
	}

	IEnumerator Start() {
		if (Constants.downloadVideo) {
			WWW www = new WWW (Constants.videoURL);
			yield return www;
			System.IO.File.WriteAllBytes (Application.streamingAssetsPath + "/video.mp4", www.bytes);
			Player.url = Application.streamingAssetsPath + "/video.mp4";
		} else {
			Player.url = Constants.videoURL;
			Player.skipOnDrop = true;
		}
		if (Player != null) {
			Player.Prepare();
		}
		index = 0;
		output = "";
		chooseImg = false;
		prepared = false;
		editing = false;
		gameData = new Serialization.EntireInputJSON ();
		gameDataList = new List<Serialization.InputJSON> ();
		images = new List<byte[]> ();
		names = new List<string> ();
		fb.guiSkin = skin;
		fb.fileTexture = file; 
		fb.directoryTexture = folder;
		fb.backTexture = back;
		fb.driveTexture = drive;
		fb.showSearch = true;
		fb.searchRecursively = true;
	}

	void Update() {
		Player.Prepare();
		Player.prepareCompleted += VideoReady;
		if (Input.GetKey (KeyCode.N) && prepared) {
			if (Input.GetMouseButtonDown (0)) {
				Player.Pause ();
				editing = true;
				menu.transform.position = m_camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.3f));
				menu.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
				menu.SetActive (true);
			}
		}
		if ((!Player.isPrepared || !Player.isPlaying || !prepared)) {
			pauseSprite.SetActive(false);
			playSprite.SetActive(true);
		} else if (Player.isPrepared && Player.isPlaying && prepared) {
			pauseSprite.SetActive(true);
			playSprite.SetActive(false);
		}

		if (Player.isPrepared && prepared) {
			if (basePosition == Vector3.zero) {
				basePosition = videoScrubber.handleRect.localPosition;
			}
			videoScrubber.maxValue = (float)(Player.frameCount) / Player.frameRate*1000;
			videoScrubber.value = (float)Player.time*1000;

			videoPosition.text = FormatTime((long)Player.time*1000);
			videoDuration.text = FormatTime((long)((float)(Player.frameCount) / Player.frameRate*1000));

		} else {
			videoScrubber.value = 0;
		}

		if (Input.GetKeyDown (KeyCode.Space) && prepared && !editing) {
			OnPlayPause ();
		}

		if (Input.GetKeyDown (KeyCode.RightArrow) && !editing) {
			skip ();
		}

		if (Input.GetKeyDown (KeyCode.LeftArrow) && !editing) {
			skipBack ();
		}
	}

	void OnGUI(){
		if (chooseImg && output == "") {
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.Confined;
			if (fb.draw ()) {
				output = (fb.outputFile == null) ? "" : fb.outputFile.ToString ();
			}
		} else if (chooseImg) {
			chooseImg = false;
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
			image ();
		}
	}

	void VideoReady(VideoPlayer vp) {
		playBtn.SetActive (true);
		loading.SetActive (false);
		prepared = true;
	}

	void OnPlayPause() {
		bool isPaused = !(Player.isPlaying);
		if (isPaused) {
			Player.Play();
		} else {
			Player.Pause();
		}
		pauseSprite.SetActive(isPaused);
		playSprite.SetActive(!isPaused);
	}

	void skip() {
		Player.Pause ();
		loading.SetActive (true);
		playBtn.SetActive (false);
		Player.time = Player.time + 1;
		Player.seekCompleted += doneSeek;
	}

	void skipBack() {
		Player.Pause ();
		loading.SetActive (true);
		playBtn.SetActive (false);
		Player.time = Player.time - 1;
		Player.seekCompleted += doneSeek;
	}

	void doneSeek(VideoPlayer vp) {
		loading.SetActive (false);
		playBtn.SetActive (true);
		Player.Play ();
	}

	public void input() {
		inputField.transform.position = menu.transform.position;
		inputField.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		inputField.SetActive (true);
		menu.SetActive (false);
	}

	public void text() {
		inputField.SetActive (false);
		GameObject newTextObj = Instantiate (textObj, canvas.transform);
		newTextObj.name = "text_"+index;
		newTextObj.transform.position = menu.transform.position;
		newTextObj.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		newTextObj.transform.GetComponent<Text> ().text = inputFieldObj.text;
		inputFieldObj.text = "";
		newTextObj.transform.GetChild (0).name = newTextObj.name;
		newTextObj.SetActive (true);
		Serialization.InputJSON newText = new Serialization.InputJSON ();
		newText.time = (long) (Player.time * 1000);
		newText.name = newTextObj.name;
		newText.type = "Text";
		newText.posX = newTextObj.transform.position.x;
		newText.posY = newTextObj.transform.position.y;
		newText.posZ = newTextObj.transform.position.z;
		newText.rotX = newTextObj.transform.eulerAngles.x;
		newText.rotY = newTextObj.transform.eulerAngles.y;
		newText.rotZ = newTextObj.transform.eulerAngles.z;
		newText.text = newTextObj.transform.GetComponent<Text> ().text;
		gameDataList.Add (newText);
		++index;
		editing = false;
	}

	public void path() {
		output = "";
		chooseImg = true;
		menu.SetActive (false);
	}

	public void image() {
		GameObject newImgObj = Instantiate (imgObj, canvas.transform);
		newImgObj.name = "image_"+index;
		newImgObj.transform.position = menu.transform.position;
		newImgObj.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		WWW localFile = new WWW ((new System.Uri(fb.currentDirectory.FullName+"/"+fb.outputFile.Name)).AbsoluteUri);
		Rect rec = new Rect(0, 0, localFile.texture.width, localFile.texture.height);
		Sprite newSprite = Sprite.Create (localFile.texture, rec, new Vector2 (0.5f, 0.5f));
		images.Add (localFile.texture.EncodeToPNG());
		newImgObj.transform.GetComponent<Image> ().sprite = newSprite;
		newImgObj.transform.GetChild (0).name = newImgObj.name;
		newImgObj.SetActive (true);
		Serialization.InputJSON newImg = new Serialization.InputJSON ();
		newImg.time = (long) (Player.time * 1000);
		newImg.name = newImgObj.name;
		newImg.type = "Image";
		newImg.posX = newImgObj.transform.position.x;
		newImg.posY = newImgObj.transform.position.y;
		newImg.posZ = newImgObj.transform.position.z;
		newImg.rotX = newImgObj.transform.eulerAngles.x;
		newImg.rotY = newImgObj.transform.eulerAngles.y;
		newImg.rotZ = newImgObj.transform.eulerAngles.z;
		string newName = (images.Count - 1) + ".png";
		newImg.file = newName;
		names.Add (newName);
		gameDataList.Add (newImg);
		++index;
		editing = false;
	}

	public void editQuestion() {
		questionEditor.transform.position = menu.transform.position;
		questionEditor.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		questionEditor.SetActive (true);
		menu.SetActive (false);
	}

	public void displayQuestion() {
		questionEditor.SetActive (false);
		questionObj.transform.position = menu.transform.position;
		questionObj.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		question.text = questionInput.text;
		answer1.GetComponentsInChildren<Text>()[0].text = answer1Input.text;
		answer2.GetComponentsInChildren<Text>()[0].text = answer2Input.text;
		answer3.GetComponentsInChildren<Text>()[0].text = answer3Input.text;
		questionObj.SetActive (true);
		Serialization.InputJSON newQuestion = new Serialization.InputJSON ();
		newQuestion.time = (long) (Player.time * 1000);
		newQuestion.type = "Question";
		newQuestion.posX = questionObj.transform.position.x;
		newQuestion.posY = questionObj.transform.position.y;
		newQuestion.posZ = questionObj.transform.position.z;
		newQuestion.rotX = questionObj.transform.eulerAngles.x;
		newQuestion.rotY = questionObj.transform.eulerAngles.y;
		newQuestion.rotZ = questionObj.transform.eulerAngles.z;
		newQuestion.question = questionInput.text;
		newQuestion.answer1 = answer1Input.text;
		newQuestion.answer2 = answer2Input.text;
		newQuestion.answer3 = answer3Input.text;
		questionInput.text = "";
		answer1Input.text = "";
		answer2Input.text = "";
		answer3Input.text = "";
		gameDataList.Add (newQuestion);
		++index;
		editing = false;
	}

	public void remove(GameObject button) {
		canvas.transform.Find (button.name).gameObject.SetActive (false);
		Serialization.InputJSON newDelete = new Serialization.InputJSON ();
		newDelete.time = (long) (Player.time * 1000);
		newDelete.type = "Destroy";
		newDelete.name = button.name;
		gameDataList.Add (newDelete);
		++index;
	}

	public void answered() {
		StartCoroutine (wait ());
	}

	IEnumerator wait() {
		yield return new WaitForSeconds (1);
		answer1.interactable = true;
		answer2.interactable = true;
		answer3.interactable = true;
		questionObj.SetActive (false);
	}

	public void Fade(bool show) {
		if (show) {
			StartCoroutine(DoAppear());
		} else {
			StartCoroutine(DoFade());
		}
	}

	IEnumerator DoAppear() {
		CanvasGroup cg = GetComponent<CanvasGroup>();
		while (cg.alpha < 1.0) {
			cg.alpha += Time.deltaTime * 2;
			yield return null;
		}
		cg.interactable = true;
		yield break;
	}

	IEnumerator DoFade() {
		CanvasGroup cg = GetComponent<CanvasGroup>();
		while (cg.alpha > 0) {
			cg.alpha -= Time.deltaTime;
			yield return null;
		}
		cg.interactable = false;
		yield break;
	}

	private string FormatTime(long ms) {
		int sec = ((int)(ms / 1000L));
		int mn = sec / 60;
		sec = sec % 60;
		int hr = mn / 60;
		mn = mn % 60;
		if (hr > 0) {
			return string.Format("{0:00}:{1:00}:{2:00}", hr, mn, sec);
		}
		return string.Format("{0:00}:{1:00}", mn, sec);
	}

	static int SortByTime(Serialization.InputJSON i1, Serialization.InputJSON i2)
	{
		return i1.time.CompareTo(i2.time);
	}
		
	public void save(Button thisBtn) {
		StartCoroutine(Upload(thisBtn));
	}

	public void exit() {
		Application.Quit();
	}

	IEnumerator Upload(Button thisBtn) {
		gameDataList.Sort (SortByTime);
		gameData.input = gameDataList;

		string time = System.DateTime.Now.GetHashCode().ToString();

		WWWForm form = new WWWForm ();
		form.AddField ("name", Constants.projectName);
		form.AddField ("video", Constants.videoURL);
		form.AddField ("data", JsonUtility.ToJson(gameData).ToString());
		form.AddField ("create_id", time);

		UnityWebRequest www = UnityWebRequest.Post (Constants.Host + "/insert.php", form);
		yield return www.Send ();

		bool error = false;

		for (int i = 0; i < images.Count; i++) {
			WWWForm form2 = new WWWForm ();
			form2.AddField ("create_id", time);
			form2.AddField ("name", names [i]);
			form2.AddBinaryData ("image", images[i], i + ".png", "image/png");
			UnityWebRequest www2 = UnityWebRequest.Post (Constants.Host + "/image.php", form2);
			yield return www2.Send ();
			if (www2.isNetworkError) {
				error = true;
			}
		}
			
		if (www.isNetworkError || error) {
			Debug.Log(www.error);
			ColorBlock cb = thisBtn.colors;
			cb.disabledColor = Color.red;
			thisBtn.colors = cb;
			thisBtn.interactable = false;
			yield return new WaitForSeconds (3);
			thisBtn.interactable = true;
		}
		else {
			Debug.Log("Form upload complete!");
			ColorBlock cb = thisBtn.colors;
			cb.disabledColor = Color.green;
			thisBtn.colors = cb;
			thisBtn.interactable = false;
			yield return new WaitForSeconds (3);
			thisBtn.interactable = true;
		}
	}
}
